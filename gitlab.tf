provider "hcloud" { }

resource "hcloud_server" "gitlab" {
    name        = "gitlab-${var.gitlab_id}"
    image       = "ubuntu-18.04"
    server_type = "${var.gitlab_instance_type}"
    location    = "nbg1"
    ssh_keys    = ["johann@sif.lordran.net", "johann@online"]

    provisioner "remote-exec" {
        inline = [
            #"apt-get update", # Useless with apt.systemd.daily
            "while pidof -x /usr/lib/apt/apt.systemd.daily > /dev/null; do sleep 1; done", # Wait for apt.systemd.daily to finish, to avoid locking issues
            "apt-get -y upgrade",
            "DEBIAN_FRONTEND='noninteractive' apt-get install -y curl ca-certificates postfix",
            "curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | bash",
            "EXTERNAL_URL='https://source.lordran.net/' apt-get install gitlab-ce",
            "echo \"letsencrypt['contact_emails'] = ['root@lordran.net']\" >> /etc/gitlab/gitlab.rb",
            "echo \"registry_external_url 'https://source.lordran.net:4567'\" >> /etc/gitlab/gitlab.rb"
        ]
    }
}

output "gitlab_public_ip" {
    value = "${hcloud_server.gitlab.ipv4_address} - ${hcloud_server.gitlab.ipv6_address}"
}
