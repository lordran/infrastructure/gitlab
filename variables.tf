variable "gitlab_id" {
    default = "prod"
}

variable "gitlab_instance_type" {
    default = "cx21-ceph"
}
